# Updating

## How to publish a version for use by [version application in Code Quality](https://gitlab.com/gitlab-services/version-gitlab-com/-/blob/c6bd7805403c919e90337de94844f150ec0887b3/.gitlab-ci.yml#L8-9)

In general, we will use our own branch, `master-gitlab` since this is a pull mirror from the original codeclimate repo.
This will allow us to update the local repo without trashing our changes.  We can then merge those updates into the
`master-gitlab` branch as needed.
   
### Adding/Updating Rubocop 

* Branch from `master-gitlab`.
* Modify [engines file](config/engines.yml) to add or modify a rubocop version.
   * Modification [example](https://gitlab.com/gitlab-org/version-app-codeclimate/-/commit/f088fe26fcdbfc859a91bd81dd1ba11b1b9367ef).
   * Addition of a new channel [example](https://gitlab.com/gitlab-org/version-app-codeclimate/-/commit/2329d1300ffe336d3257c6ea1ff6d3e0a3bbbacf).
   * The channels refer to a docker image with tag that was published by following the [updating guide for version-app-codeclimate-rubocop](https://gitlab.com/gitlab-org/version-app-codeclimate-rubocop/-/blob/gitlab/rubocop-0-82/UPDATING.md).
* Optional: run `make test`
   * Note: CI will run this step as well..in case you don't have docker installed locally.
* Open Merge Request 

### Creating a new image

* Once MR is merged, add the appropriate Tag by going [here](https://gitlab.com/gitlab-org/version-app-codeclimate/-/tags).
* Tag must be in format as seen in the [.gitlab-ci.yml](.gitlab-ci.yml) under `release-version`/`rules`
* Adding that tag will then kick off a pipeline that will test and publish the new image with that tag [here](https://gitlab.com/gitlab-org/version-app-codeclimate/container_registry/eyJuYW1lIjoiZ2l0bGFiLW9yZy92ZXJzaW9uLWFwcC1jb2RlY2xpbWF0ZSIsInRhZ3NfcGF0aCI6Ii9naXRsYWItb3JnL3ZlcnNpb24tYXBwLWNvZGVjbGltYXRlL3JlZ2lzdHJ5L3JlcG9zaXRvcnkvMTA1MjM2OC90YWdzP2Zvcm1hdD1qc29uIiwiaWQiOjEwNTIzNjh9).

### Using this image in [version-gitlab-com](https://gitlab.com/gitlab-services/version-gitlab-com)

Once you've created this new docker image, we can now handoff to the 
[rubocop updating instructions](https://gitlab.com/gitlab-services/version-gitlab-com/-/blob/master/README.md#updating-rubocop)
for info on how to use this image for our Code Quality invocation.